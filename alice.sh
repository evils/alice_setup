#!/usr/bin/env bash
# up to date version at gitlab.com/evils/alice_setup
if ! [ $(id -u) = 0 ]; then
   echo "Run this as root or with sudo."
   exit 1
fi &&
git clone https://github.com/analogdevicesinc/alice.git &&
cd alice &&
# change branch name if there's a new version...
git checkout Version-1.2 &&
# set up libsmu
# this installs way more of boost than is needed...
apt install python-setuptools python-pip cmake python2.7-dev pkgconf libboost-all-dev &&
pip install cython &&
git clone https://github.com/analogdevicesinc/libsmu.git &&
cd libsmu &&
mkdir build && cd build && cmake .. &&
make && make install &&
cd ../.. &&
# set up alice
apt install libusb-1.0-0-dev libudev-dev &&
pip install numpy &&
# the next two may be unneeded but should be harmless?
ldconfig &&
udevadm control --reload-rules &&
# a bit of convenience
chmod +x alice-desktop-1.2.pyw &&
# i think pixelpulse2 does this in a cleaner (or more obscure) way
# this requires a relog to become permanent/universal
echo "export PYTHONPATH=/usr/lib/python2.7/site-packages:\${PYTHONPATH}" > /etc/profile.d/pysmu.sh &&
export PYTHONPATH=/usr/lib/python2.7/site-packages:${PYTHONPATH} &&
# hopefully done
echo "
ALICE set up and libsmu installed.
Use this terminal or log back in to get the right PYTHONPATH.
Happy hacking!"
